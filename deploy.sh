#!/bin/bash

set -e
set -x

# TARGET=sgx3
TARGET=ubuntu@192.168.122.6
# TARGET=dev

DIR=$(basename $(readlink -f .))

ssh $TARGET mkdir -p $DIR

rsync -av . $TARGET:~/$DIR
ssh $TARGET "cd $DIR ; make -k"
