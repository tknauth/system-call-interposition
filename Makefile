obj-m+=syscall.o
# https://www.kernel.org/doc/Documentation/kbuild/modules.txt
ccflags-y := -g -ggdb

all:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) clean
