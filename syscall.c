#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/syscalls.h>

/* Proof of concept how to interpose system calls on Linux with a kernel module. */

/* TODO use parameter to filter by PID */
/* static int syscall_threshold = 1000; */
/* module_param(syscall_threshold, int, 0644); */

/* Kudos to http://r00tkit.me/?p=46 for the overall outline how to do it. */

typedef long (*sys_call_ptr_t)(long, long, long,
                               long, long, long);

static sys_call_ptr_t *sys_call_table = NULL;

asmlinkage int (*original_write)(unsigned int, const char __user *, size_t);

asmlinkage int new_write(unsigned int fd, const char __user *buf, size_t count){
    struct task_struct* ts = current;

    printk(KERN_INFO "write %s pid=%d %d %p %zd\n", ts->comm, ts->pid, fd, buf, count);
    return (*original_write)(fd, buf, count);
}


static void change_write_call(void) {
    /* TODO disable interrupts */
    write_cr0 (read_cr0 () & (~ 0x10000));

    original_write = (void *)sys_call_table[__NR_write];

    sys_call_table[__NR_write] = (sys_call_ptr_t*) new_write;

    printk(KERN_EMERG "Write system call old address: %p\n", original_write);
    printk(KERN_EMERG "Write system call new address: %p\n", new_write);

    //Changing control bit back
    write_cr0 (read_cr0 () | 0x10000);
}

/* https://bbs.archlinux.org/viewtopic.php?id=139406 */
static sys_call_ptr_t *aquire_sys_call_table(void)
{
	unsigned long int offset = PAGE_OFFSET;
	unsigned long **sct;
	unsigned long i = 0;

	while (offset < ULLONG_MAX) {
		sct = (unsigned long **)offset;

		if (sct[__NR_close] == (unsigned long *) sys_close) {
		  printk(KERN_INFO "%lu iterations to find sys_call_table\n", i);
		  return (sys_call_ptr_t*) sct;
		}

		offset += sizeof(void *);
		++i;
	}
	
	return NULL;
}

static int __init my_module_init(void){
   
   printk(KERN_INFO "Loaded!\n");
   sys_call_table = aquire_sys_call_table();
   if (sys_call_table == NULL) return 1;

   change_write_call();
   return 0;
}

static void __exit my_module_exit(void){
   /* TODO disable interrupts */
   write_cr0 (read_cr0 () & (~ 0x10000));
   sys_call_table[__NR_write] = original_write;
   write_cr0 (read_cr0 () | 0x10000);
   printk(KERN_EMERG "Module exited cleanly");
   return;
}

module_init(my_module_init);
module_exit(my_module_exit);
